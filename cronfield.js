/**
 * @file
 * Render and update products associated to the current page in the catalog.
 */

(function ($) {
  Drupal.behaviors.catalogPageProductsMaintainer = {
    attach: function (context, settings) {
        
      $('.cron-generate-btn').click(function(){
          
          var cronValue = generateCron();
          $(this).parent().parent().parent().find('.cron-field-input-form-field').val(cronValue);
         
      });
      
      $('.chooser').click(function(){
        
        var name = $(this).attr('name');
        if($(this).hasClass('chooser-every')){
          // OFF.
          $(this).parent().parent().find('select.' + name).attr('disabled','disabled');
        }
        if($(this).hasClass('chooser-choos')){
          // ON.
          $(this).parent().parent().find('select.' + name).removeAttr('disabled');
        }
        
      });
      
      function generateCron() {
          
        var minute	= getSelection('minute');
        var hour	= getSelection('hour');
        var day		= getSelection('day');
        var month	= getSelection('month');
        var weekday	= getSelection('weekday');
          
        return minute + ' ' + hour + ' ' + day + ' ' + month + ' ' + weekday;
      
      }
        
      function getSelection(name) {
        var chosen;
        
        if($('input[name=' + name + ']:checked', '.cron-' + name ).val() == 0) {
          chosen = '*';
        }else{
          
          var all_selected = [];
           
          $('.cron-' + name +' select.' + name + ' option:selected').each(function(i, selected) {
            all_selected.push($(selected).val());
          }); 
          
          if(all_selected.length){
            chosen = all_selected.join(",");
          }
          else{
            chosen = '*';
          }        
        
        }
        
        return chosen;
      }

    }
  };
})(jQuery);