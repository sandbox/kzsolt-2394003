<div id="cron-selector-container" >
  <div class="cron-minute cron-box">
    <h3><?php print t('Minute'); ?></h3>

    <label for="minute_chooser_every"><?php print t('Every Minute'); ?></label>
    <input id="minute_chooser_every" type="radio" checked="checked" value="0" class="chooser chooser-every" name="minute" /><br>

    <label for="minute_chooser_choose"><?php print t('Choose'); ?></label>
    <input id="minute_chooser_choose" type="radio" value="1" class="chooser chooser-choos" name="minute" /><br>

    <select multiple="multiple" class="minute cron-select-box" disabled="disabled">
      <?php for ($i = 0; $i < 60; $i++) { ?>
        <option value="<?php print $i; ?>"><?php print $i; ?></option>
      <?php } ?>

    </select>
  </div>

  <div class="cron-hour cron-box">
    <h3><?php print t('Hour'); ?></h3>
    <label for="hour_chooser_every"><?php print t('Every Hour'); ?></label>
    <input type="radio" checked="checked" value="0" class="chooser chooser-every" id="hour_chooser_every" name="hour"/><br>

    <label for="hour_chooser_choose"><?php print t('Choose'); ?></label>
    <input type="radio" value="1" class="chooser chooser-choos" id="hour_chooser_choose" name="hour"><br>

    <select multiple="multiple" class="hour cron-select-box" disabled="disabled">
      <option value="0"><?php print t('12 Midnight'); ?></option>
      <option value="1"></option><option value="2"><?php print t('2 AM');?></option><option value="3"><?php print t('3 AM');?></option><option value="4"><?php print t('4 AM');?></option><option value="5"><?php print t('5 AM');?></option><option value="6"><?php print t('6 AM');?></option><option value="7"><?php print t('7 AM');?></option><option value="8"><?php print t('8 AM');?></option><option value="9"><?php print t('9 AM');?></option><option value="10"><?php print t('10 AM');?></option><option value="11"><?php print t('11 AM');?></option><option value="12"><?php print t('12 Noon');?></option>
      <option value="13"><?php print t('1 PM');?></option><option value="14"><?php print t('2 PM');?></option><option value="15"><?php print t('3 PM');?></option><option value="16"><?php print t('4 PM');?></option><option value="17"><?php print t('5 PM');?></option><option value="18"><?php print t('6 PM');?></option><option value="19"><?php print t('7 PM');?></option><option value="20"><?php print t('8 PM');?></option><option value="21"><?php print t('9 PM');?></option><option value="22"><?php print t('10 PM');?></option><option value="23"><?php print t('11 PM');?></option>
    </select>
  </div>

  <div class="cron-day cron-box">
    <h3><?php print t('Day'); ?></h3>
    <label for="day_chooser_every"><?php print t('Every Day'); ?></label>
    <input type="radio" checked="checked" value="0" class="chooser chooser-every" id="day_chooser_every" name="day"><br>

    <label for="day_chooser_choose"><?php print t('Choose'); ?></label>
    <input type="radio" value="1" class="chooser chooser-choos" id="day_chooser_choose" name="day"><br>

    <select  multiple="multiple" class="day cron-select-box"  disabled="disabled">
      <?php for ($i = 1; $i < 31; $i++) { ?>
        <option value="<?php print $i; ?>"><?php print $i; ?></option>
      <?php } ?>

    </select>
  </div>

  <div class="cron-month cron-box">
    <h3><?php print t('Month'); ?></h3>
    <label for="month_chooser_every"><?php print t('Every Month'); ?></label>
    <input type="radio" checked="checked" value="0" class="chooser chooser-every" id="month_chooser_every" name="month"><br>

    <label for="month_chooser_choose"><?php print t('Choose'); ?></label>
    <input type="radio" value="1" class="chooser chooser-choos" id="month_chooser_choose" name="month"><br>

    <select multiple="multiple" class="month cron-select-box" disabled="disabled">
      <option value="1"><?php print t('January'); ?></option>
      <option value="2"><?php print t('February'); ?></option>
      <option value="3"><?php print t('March'); ?></option>
      <option value="4"><?php print t('April'); ?></option>
      <option value="5"><?php print t('May'); ?></option>
      <option value="6"><?php print t('June'); ?></option>
      <option value="7"><?php print t('July'); ?></option>
      <option value="8"><?php print t('Augest'); ?></option>
      <option value="9"><?php print t('September'); ?></option>
      <option value="10"><?php print t('October'); ?></option>
      <option value="11"><?php print t('November'); ?></option>
      <option value="12"><?php print t('December'); ?></option>
    </select>
  </div>

  <div class="cron-weekday cron-box">
    <h3><?php print t('Weekday'); ?></h3>
    <label for="weekday_chooser_every"><?php print t('Every Weekday'); ?></label>
    <input type="radio" checked="checked" value="0" class="chooser chooser-every" id="weekday_chooser_every" name="weekday"><br>

    <label for="weekday_chooser_choose"><?php print t('Choose'); ?></label>
    <input type="radio" value="1" class="chooser chooser-choos" id="weekday_chooser_choose" name="weekday"><br>

    <select multiple="multiple" class="weekday cron-select-box" disabled="disabled">
      <option value="0"><?php print t('Sunday'); ?></option>
      <option value="1"><?php print t('Monday'); ?></option>
      <option value="2"><?php print t('Tuesday'); ?></option>
      <option value="3"><?php print t('Wednesday'); ?></option>
      <option value="4"><?php print t('Thursday'); ?></option>
      <option value="5"><?php print t('Friday'); ?></option>
      <option value="6"><?php print t('Saturday'); ?></option>
    </select>
  </div>

</div>
<div style ="clear: both;"></div>
<div>
  <input type="button" value="Generate Cron" class="cron-generate-btn"/>
</div>
